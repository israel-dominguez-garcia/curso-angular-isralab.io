import { Component } from '@angular/core';
import { ActivatedRoute } from "@angular/router";
import { ProductosService } from "../../services/productos.service";

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html'
})
export class SearchComponent {

  q:string = undefined;

  constructor(private route:ActivatedRoute,
              public _ps:ProductosService) {
    route.params.subscribe( parametros=>{
      this.q = parametros['q'];
      _ps.search_product(this.q);
    });
  }
}
