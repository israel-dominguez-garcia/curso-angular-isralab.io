import { Injectable } from '@angular/core';
import {Http} from "@angular/http";

@Injectable()
export class ProductosService {

  productos:any[] = [];
  loaded:boolean = false;
  productos_search:any[] = [];

  constructor(private http:Http) {
    this.load_productos();
  }

  public load_producto(cod:string) {
      return this.http.get(`https://curso-angular-a76a9.firebaseio.com/productos/${cod}.json`);
  }

  public load_productos() {
    if (this.productos.length === 0) {
      //Angular Fire para hacer queries

        let promesa = new Promise((resolve, reject)=> {
            this.http.get('https://curso-angular-a76a9.firebaseio.com/productos_idx.json')
                .subscribe(data => {
                    //console.log(data.json());
                    this.loaded = true;
                    this.productos = data.json();
                    resolve();
                });
        });
        return promesa;
    }
  }

  public search_product(q:string) {
      if (this.productos.length ===0) {
          this.load_productos().then(()=>{
              //termino la carga
              this.filter_product(q);
          });
      } else {
          this.filter_product(q);
      }
  }

  private filter_product(q:string) {
      q = q.toLowerCase();
      this.productos_search = [];
      this.productos.forEach( prod => {
          if (prod.categoria.indexOf(q)>=0 || prod.titulo.toLowerCase().indexOf(q)>=0){
              this.productos_search.push(prod);
          }
      });
  }
}
