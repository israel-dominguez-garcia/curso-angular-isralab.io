import { Injectable } from '@angular/core';
import { Http } from "@angular/http"

@Injectable()
export class InformacionService {

  info:any = {};
  loaded:boolean = false;
  loaded_sobre:boolean = false;
  team:any = [];

  constructor( public http:Http) {
      this.load_info();
      this.load_sobre_nosotros();
  }

  public load_info() {
      this.http.get("assets/data/info.pagina.json")
          .subscribe( data =>{
              //console.log(data.json());
              this.info = data.json();
              this.loaded = true;
          });
  }

  public load_sobre_nosotros() {
      this.http.get("https://curso-angular-a76a9.firebaseio.com/equipo.json")
          .subscribe( data =>{
              //console.log(data.json());
              this.team = data.json();
              this.loaded_sobre = true;
          });
  }
}
